package bitserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import bitserver.entity.Entity;
import bitserver.entity.Player;
import bitserver.javaplugin.PluginLoader;
import bitserver.logger.Logger;
import bitserver.nms.Loader;
import bitserver.world.World;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * The entry-point of the program
 */
public class BitServer {

	/**
	 * Constructs the BitServer
	 * 
	 * @param args
	 *            the arguments passed to the server
	 * @return Nothing
	 * @see MinecraftServer
	 */
	public static void main(String[] args) {
		new BitServer(args);
	}

	private static List<Player> players = new ArrayList<Player>();
	private static Logger l = Logger.getLogger(BitServer.class);
	public static PluginLoader pluginLoader;
	private static HashMap<String, World> worlds = new HashMap<String, World>();
	private static HashMap<UUID, Entity> entities = new HashMap<UUID, Entity>();

	/**
	 * @param args
	 *            the arguments to pass
	 */

	public BitServer(String[] args) {
		pluginLoader = new PluginLoader();
		l.logln("Loading minecraft server");
		Loader nmsLoader = new Loader(args);

		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				PluginLoader.unLoadAllPlugins();
			}

		});

	}

	public static Player getPlayer(String name) {
		for (Player p : players) {
			if (p.getName() == name) {
				return p;
			}
		}
		return null;
	}

	public static Player getPlayer(UUID uuid) {
		for (Player p : players) {
			if (p.getUniqueId() == uuid) {
				return p;
			}
		}
		return null;
	}

	public static List<Player> getOnlinePlayers() {
		return players;
	}

	public static void a(Class<?> p) {
		players.add(new Player(p));
		l.logln("Added player:" + players.get(players.size() - 1).getName());
	}

	public static World getWorld(String name) {
		return worlds.get(name);
	}

	public static void addEntity(Entity e) {
		entities.put(e.getUniqueId(), e);
	}

	public static Entity getEntity(UUID id) {
		return entities.get(id);
	}

	public static void remove(UUID id) {
		entities.remove(id);
	}
}
