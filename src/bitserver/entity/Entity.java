package bitserver.entity;

import java.util.UUID;

import bitserver.BitServer;
import bitserver.nms.Classes;
import bitserver.reflection.ReflectionUtil;
import bitserver.util.Location;
import bitserver.util.Vector;
import bitserver.world.World;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public class Entity {

	protected Class<?> nms;

	public Entity(Class<?> nms) {
		this.nms = nms;
	}

	public Location getLocation() {
		try {
			double x = (double) ReflectionUtil.getField(
					Classes.Entity.className, nms, "s");
			double y = (double) ReflectionUtil.getField(
					Classes.Entity.className, nms, "t");
			double z = (double) ReflectionUtil.getField(
					Classes.Entity.className, nms, "u");
			String worldName = (String) ReflectionUtil.getField(
					Classes.WorldInfo.className,
					ReflectionUtil.getField(
							Classes.World.className,
							ReflectionUtil.getField(Classes.Entity.className,
									nms, "c").getClass(), "x").getClass(), "m");
			Location loc = new Location(BitServer.getWorld(worldName), x, y, z);
			float yaw = (float) ReflectionUtil.getField(
					Classes.Entity.className, nms, "y");
			float pitch = (float) ReflectionUtil.getField(
					Classes.Entity.className, nms, "z");
			loc.setYaw(yaw);
			loc.setPitch(pitch);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new Location(null, 0, 0, 0);
	}

	public void setLocation(Location loc) {
		// ReflectionUtil.setField(Classes.Entity.className, nms, "s",
		// loc.getX());
		// ReflectionUtil.setField(Classes.Entity.className, nms, "t",
		// loc.getY());
		// ReflectionUtil.setField(Classes.Entity.className, nms, "u",
		// loc.getZ());
		ReflectionUtil.getMethod(Classes.Entity.className, nms, "b",
				loc.getX(), loc.getY(), loc.getZ());
		ReflectionUtil.setField(Classes.Entity.className, nms, "y",
				loc.getYaw());
		ReflectionUtil.setField(Classes.Entity.className, nms, "z",
				loc.getPitch());
		ReflectionUtil.setField(Classes.Entity.className, nms, "c",
				loc.getWorld().nms);
	}

	public World getWorld() {
		String worldName = (String) ReflectionUtil.getField(
				Classes.WorldInfo.className,
				ReflectionUtil.getField(
						Classes.World.className,
						ReflectionUtil.getField(Classes.Entity.className, nms,
								"c").getClass(), "x").getClass(), "m");
		return BitServer.getWorld(worldName);
	}

	public Vector getVelocity() {
		double x = (double) ReflectionUtil.getField(Classes.Entity.className,
				nms, "v");
		double y = (double) ReflectionUtil.getField(Classes.Entity.className,
				nms, "w");
		double z = (double) ReflectionUtil.getField(Classes.Entity.className,
				nms, "x");
		return new Vector(x, y, z);
	}

	public void setVelocity(Vector v) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "v", v.getX());
		ReflectionUtil.setField(Classes.Entity.className, nms, "w", v.getY());
		ReflectionUtil.setField(Classes.Entity.className, nms, "x", v.getZ());
		ReflectionUtil.setField(Classes.Entity.className, nms, "H", true);
	}

	public boolean isOnGround() {
		return (boolean) ReflectionUtil.getField(Classes.Entity.className, nms,
				"D");
	}

	public boolean isInWeb() {
		return (boolean) ReflectionUtil.getField(Classes.Entity.className, nms,
				"I");
	}

	public boolean isDead() {
		return (boolean) ReflectionUtil.getField(Classes.Entity.className, nms,
				"K");
	}

	public float getYOffset() {
		return (float) ReflectionUtil.getField(Classes.Entity.className, nms,
				"L");
	}

	public void setYOffset(float offset) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "L", offset);
	}

	public boolean noClip() {
		return (boolean) ReflectionUtil.getField(Classes.Entity.className, nms,
				"X");
	}

	public void setNoClip(boolean noClip) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "X", noClip);
	}

	public int ticksAlive() {
		return (int) ReflectionUtil.getField(Classes.Entity.className, nms,
				"aa");
	}

	public int fireResistance() {
		return (int) ReflectionUtil.getField(Classes.Entity.className, nms,
				"ab");
	}

	public void setFireResistance(int i) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "ab", i);
	}

	public double getEntityRiderPitchDelta() {
		return (double) ReflectionUtil.getField(Classes.Entity.className, nms,
				"g");
	}

	public void setEntityRiderPitchDelta(double d) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "g", d);
	}

	public double getEntityRiderYawDelta() {
		return (double) ReflectionUtil.getField(Classes.Entity.className, nms,
				"h");
	}

	public void setEntityRiderYawDelta(double d) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "h", d);
	}

	public boolean isInvulnerable() {
		return (boolean) ReflectionUtil.getField(Classes.Entity.className, nms,
				"i");
	}

	public void setInvulnerable(boolean b) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "i", b);
	}

	public void setEntitySize(EntitySize size) {
		ReflectionUtil
				.setField(Classes.Entity.className, nms, "as",
						((Object[]) ReflectionUtil.getField(
								Classes.Entity$EnumEntitySize.className, null,
								"g"))[size.size]);
	}

	public EntitySize getEntitySize() {
		return EntitySize.values()[(int) ReflectionUtil.getMethod(
				Classes.Entity$EnumEntitySize.className, ReflectionUtil
						.getField(Classes.Entity.className, nms, "as")
						.getClass(), "ordinal") - 1];
	}

	public UUID getUniqueId() {
		return (UUID) ReflectionUtil.getField(Classes.Entity.className, nms,
				"ar");
	}

	public void setUniqueId(UUID uuid) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "ar", uuid);
	}

	public int getId() {
		return (int) ReflectionUtil
				.getField(Classes.Entity.className, nms, "c");
	}

	public void setId(int id) {
		ReflectionUtil.setField(Classes.Entity.className, nms, "c", id);
	}

	public Entity getPassenger() {
		return BitServer.getEntity((UUID) ReflectionUtil.getField(
				Classes.Entity.className,
				ReflectionUtil.getField(Classes.Entity.className, nms, "l"),
				"ar"));
	}

	public Entity getVehicle() {
		return BitServer.getEntity((UUID) ReflectionUtil.getField(
				Classes.Entity.className,
				ReflectionUtil.getField(Classes.Entity.className, nms, "m"),
				"ar"));
	}

	public void setVehicle(Entity e) {
		ReflectionUtil.getMethod(Classes.Entity.className, nms, "a", e.nms);
	}

	public void setPassenger(Entity e) {
		e.setVehicle(this);
	}

	public boolean isSneaking() {
		return (boolean) ReflectionUtil.getMethod(Classes.Entity.className,
				nms, "an");
	}

	public void setSneaking(boolean b) {
		ReflectionUtil.getMethod(Classes.Entity.className, nms, "b", b);
	}

	public boolean isSprinting() {
		return (boolean) ReflectionUtil.getMethod(Classes.Entity.className,
				nms, "ao");
	}

	public void setSprinting(boolean b) {
		ReflectionUtil.getMethod(Classes.Entity.className, nms, "c", b);
	}

	public boolean isInvisible() {
		return (boolean) ReflectionUtil.getMethod(Classes.Entity.className,
				nms, "ap");
	}

	public void setInvisible(boolean b) {
		ReflectionUtil.getMethod(Classes.Entity.className, nms, "d", b);
	}

	public static enum EntitySize {
		SIZE_1(0), SIZE_2(1), SIZE_3(2), SIZE_4(3), SIZE_5(4), SIZE_6(5);
		int size;

		EntitySize(int size) {
			this.size = size;
		}
	}
}
