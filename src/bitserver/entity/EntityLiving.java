package bitserver.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bitserver.BitServer;
import bitserver.item.ItemStack;
import bitserver.nms.Classes;
import bitserver.reflection.ReflectionUtil;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public class EntityLiving extends Entity {

	public EntityLiving(Class<?> nms) {
		super(nms);
	}

	public float getAttackedAtYaw() {
		return (float) ReflectionUtil.getField(
				Classes.EntityLivingBase.className, nms, "az");
	}

	public float getHeadYaw() {
		return (float) ReflectionUtil.getField(
				Classes.EntityLivingBase.className, nms, "aO");
	}

	public int getAge() {
		return (int) ReflectionUtil.getField(
				Classes.EntityLivingBase.className, nms, "aU");
	}

	public void setAge(int age) {
		ReflectionUtil.setField(Classes.EntityLivingBase.className, nms, "aU",
				age);
	}

	public ItemStack[] getEquipment() {
		List<ItemStack> l = new ArrayList<ItemStack>();
		for (Object is : ((Object[]) ReflectionUtil.getField(
				Classes.EntityLiving.className, nms, "br"))) {
			l.add(new ItemStack(is.getClass()));
		}
		return (ItemStack[]) l.toArray();
	}

	public void setEquipment(ItemStack[] is) {
		Object[] obj = new Object[5];
		for (int i = 0; i < 5; i++) {
			if (i < is.length) {
				obj[i] = is[i].nms;
			} else {
				obj[i] = null;
			}
		}
		ReflectionUtil.setField(Classes.EntityLiving.className, nms, "br", obj);
	}

	public boolean canPickupLoot() {
		return (boolean) ReflectionUtil.getField(
				Classes.EntityLiving.className, nms, "bs");
	}

	public void setCanPickupLoot(boolean b) {
		ReflectionUtil.setField(Classes.EntityLiving.className, nms, "bs", b);
	}

	public Entity getLeashHolder() {
		return BitServer.getEntity((UUID) ReflectionUtil.getField(
				Classes.Entity.className,
				ReflectionUtil.getField(Classes.EntityLiving.className, nms,
						"bw").getClass(), "ar"));
	}

	public void setLeashHolder(Entity leashHolder) {
		ReflectionUtil.getMethod(Classes.EntityLiving.className, nms, "b",
				leashHolder.nms, true);
	}

	public void setCustomName(String name) {
		ReflectionUtil
				.getMethod(Classes.EntityLiving.className, nms, "a", name);
	}

	public String getCustomName() {
		return (String) ReflectionUtil.getMethod(
				Classes.EntityLiving.className, nms, "bG");
	}

	public void setCustomNameVisible(boolean b) {
		ReflectionUtil.getMethod(Classes.EntityLiving.className, nms, "g", b);
	}

	public void setHealth(float health) {
		ReflectionUtil.getMethod(Classes.EntityLivingBase.className, nms, "g",
				health);
	}

	public final float getHealth() {
		return (float) ReflectionUtil.getMethod(
				Classes.EntityLivingBase.className, nms, "aS");
	}
	
	public final float getMaxHealth(){
		return (float) ReflectionUtil.getMethod(Classes.EntityLivingBase.className, nms, "aY");
	}
	
	public void setMaxHealth(float mHealth){
		ReflectionUtil.setField(Classes.EntityLivingBase.className, nms, "setMaxHealth", mHealth);
	}

}
