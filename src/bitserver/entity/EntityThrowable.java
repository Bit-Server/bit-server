package bitserver.entity;

import java.util.UUID;

import bitserver.BitServer;
import bitserver.block.Block;
import bitserver.nms.Classes;
import bitserver.reflection.ReflectionUtil;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class EntityThrowable extends Entity {

	/**
	 * @param nms
	 */
	public EntityThrowable(Class<?> nms) {
		super(nms);
	}

	public Entity getThrower() {
		return BitServer.getEntity((UUID) ReflectionUtil.getField(
				Classes.Entity.className,
				ReflectionUtil.getField(Classes.EntityThrowable.className, nms,
						"g").getClass(), "ar"));
	}

	public Block getBlockIAmIn() {
		return new Block(ReflectionUtil.getField(
				Classes.EntityThrowable.className, nms, "f").getClass());
	}

}
