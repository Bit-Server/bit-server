package bitserver.entity;

import java.util.HashMap;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public enum EntityType {

	ITEM(1), EXP(2), LEAD(8), PAINTING(9), ITEM_FRAME(18), ENDER_CRYSTAL(200), ARROW(
			10), SNOWBALL(11), FIREBALL(12), FIREBALL_SMALL(13), ENDER_PEARL(14), ENDER_EYE(
			15), SPLASH_POTION(16), EXP_BOTTLE(17), WITHER_SKULL(19), FIREWORK(
			22), TNT(20), FALLING_BLOCK(21), MINECART_COMMAND(40), BOAT(41), MINECART(
			42), MINECART_CHEST(43), MINECART_FURNACE(44), MINECART_TNT(45), MINECART_HOPPER(
			46), MINECART_SPAWNER(47), CREEPER(50), SKELETON(51), SPIDER(52), GIANT(
			53), ZOMBIE(54), SLIME(55), GHAST(56), ZOMBIE_PIGMAN(57), ENDERMAN(
			58), SPIDER_CAVE(59), SILVERFISH(60), BLAZE(61), MAGMA_CUBE(62), ENDER_DRAGON(
			63), WITHER(64), WITCH(66), BAT(65), PIG(90), SHEEP(91), COW(92), CHICKEN(
			93), SQUID(94), WOLF(95), MOOSHROOM(96), GOLEM_SNOW(97), OCELOT(98), GOLEM_IRON(
			99), HORSE(100), VILLAGER(120),PLAYER(1000);

	private int id;

	EntityType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	private static HashMap<Integer, EntityType> types = new HashMap<Integer, EntityType>();

	public static EntityType getById(int id) {
		return types.get(id);
	}

	static {
		for (EntityType type : values()) {
			types.put(Integer.valueOf(type.id), type);
		}
	}

}
