package bitserver.event;

import java.lang.reflect.Method;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * The handler for firing events
 */
public class EventPublisher {
	/**
	 * Raise an event
	 * @param event the event to raise
	 */
	public static void raiseEvent(final Event event) {
		//new Thread() {
		//	@Override
		//	public void run() {
				raise(event);
		//	}
		//}.start();
	}
	/**
	 * Raise the specified event
	 * @param event the event to raise
	 */
	private static void raise(final Event event) {
		for (Class handler : HandlerRegistry.getHandlers()) {
			Method[] methods = handler.getMethods();

			for (int i = 0; i < methods.length; ++i) {
				EventHandler eventHandler = methods[i]
						.getAnnotation(EventHandler.class);
				if (eventHandler != null) {
					Class[] methodParams = methods[i].getParameterTypes();

					if (methodParams.length < 1)
						continue;

					if (!event.getClass().getSimpleName()
							.equals(methodParams[0].getSimpleName()))
						continue;
					
					try {
						methods[i].invoke(handler.newInstance(), event);
					} catch (Exception e) {
						System.err.println(e);
					}
				}
			}
		}
	}
}
