package bitserver.event;

import java.util.UUID;

import bitserver.reflection.ReflectionUtil;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public class PlayerLoginEvent extends Cancelable implements Event {

	private String name;
	private UUID id;
	private String disconnectReason;

	public String getDisconnectReason() {
		return disconnectReason;
	}

	public void setDisconnectReason(String disconnectReason) {
		this.disconnectReason = disconnectReason;
	}

	public PlayerLoginEvent(String name, UUID id) {
		this.name = name;
		this.id = id;
	}
	
	public String getPlayerName(){
		return name;
	}
	
	public UUID getPlayerId(){
		return id;
	}

}
