package bitserver.event;

import bitserver.block.Block;
import bitserver.entity.Entity;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class ProjectileShootEvent {

	private Entity projectile;
	private Entity shooter_Entity;
	private Block shooter_Block;

	public ProjectileShootEvent(Entity p, Entity se, Block sb) {
		projectile = p;
		shooter_Entity = se;
		shooter_Block = sb;
	}

	public Entity getProjectile() {
		return projectile;
	}

	public void setProjectile(Entity projectile) {
		this.projectile = projectile;
	}

	public Entity getShooter_Entity() {
		return shooter_Entity;
	}

	public void setShooter_Entity(Entity shooter_Entity) {
		this.shooter_Entity = shooter_Entity;
	}

	public Block getShooter_Block() {
		return shooter_Block;
	}

	public void setShooter_Block(Block shooter_Block) {
		this.shooter_Block = shooter_Block;
	}

}
