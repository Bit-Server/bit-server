package bitserver.javaplugin;

import bitserver.command.Command;
import bitserver.logger.Logger;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * The interface for a Plugin
 */
public abstract class JavaPlugin {
	/**
	 * Called when the plugin is enabled
	 */
	public abstract void onEnable();
	/**
	 * Called when the plugin is disabled
	 */
	public abstract void onDisable();
	/**
	 * Called when a command is issued
	 * @param command the command
	 */
	public abstract void onCommand(Command command);
	/**
	 * The PluginInfo
	 */
	protected PluginInfo pi = null;
	/**
	 * Get the plugin info
	 * @return pi
	 */
	public PluginInfo getPluginInfo() {
		return pi;
	}
	
	public void loadPluginInfo(){
		if(pi == null){
			pi = PluginInfo.getPluginInfo(PluginLoader.currentPlugin);
		}
	}
	
	private Logger logger = null;
	
	public Logger getLogger(){
		if(pi == null){
			loadPluginInfo();
		}
		if(logger == null)
			logger = Logger.getLogger(getPluginInfo());
		return logger;
	}

}
