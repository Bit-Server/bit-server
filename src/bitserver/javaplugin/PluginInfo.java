package bitserver.javaplugin;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * The class to provide info for plugins
 */
public class PluginInfo {
	
	public static HashMap<String, PluginInfo> pluginInfo = new HashMap<String, PluginInfo>();
	
	/**
	 * The name
	 */
	public String name = "";
	/**
	 * The version
	 */
	public String version = "";
	/**
	 * The main class
	 */
	public String mainClass = "";
	/**
	 * The author
	 */
	public String author = "";
	/**
	 * The description
	 */
	public String description = "";
	/**
	 * The commands
	 */
	public List<String> commands;

	/**
	 * Set the variables
	 * 
	 * @param name
	 *            the name
	 * @param version
	 *            the version
	 * @param mainClass
	 *            the main class
	 * @param author
	 *            the author
	 * @param description
	 *            the description
	 * @param commands
	 *            the commands
	 */
	protected PluginInfo(String name, String version, String mainClass,
			String author, String description, List<String> commands) {
		this.name = name;
		this.version = version;
		this.mainClass = mainClass;
		this.author = author;
		this.description = description;
		this.commands = commands;
	}

	/**
	 * Read from a YML file
	 * 
	 * @param pluginYML
	 *            the file
	 * @return a PluginInfo
	 */
	public static PluginInfo generatePluginInfo(InputStream pluginYML) {
		Yaml yaml = new Yaml();
		Map<String, Object> map = (Map<String, Object>) yaml.load(pluginYML);
		String version = map.get("version") instanceof Double ?  String.valueOf(((double)map.get("version"))) : ((String)map.get("version"));
		return new PluginInfo((String) map.get("name"),
				version, (String) map.get("mainClass"),
				(String) map.get("author"), (String) map.get("discription"),
				(List<String>) map.get("commands"));
	}
	
	public static PluginInfo getPluginInfo(String name){
		return pluginInfo.get(name);
	}
}
