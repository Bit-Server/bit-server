package bitserver.javaplugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import bitserver.logger.Logger;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Class for loading the plugins
 */
public class PluginLoader {

	private static Logger logger = Logger.getLogger(PluginLoader.class);

	/**
	 * The plugins
	 */
	private static HashMap<String, Object> plugins = new HashMap<String, Object>();

	public static String currentPlugin = "";

	/**
	 * Load the plugins
	 */
	public void loadAllPlugins() {
		logger.logln("Loading plugins, in the folder \'plugins\'");
		File dir = new File("./plugins");
		if (dir.exists()) {
			for (File f : dir.listFiles()) {
				if (f.getName().endsWith(".jar")) {
					loadPlugin(f.getName().replace(".jar", ""));
				}
			}
		} else {
			dir.mkdir();
		}
	}

	/**
	 * Load the plugin
	 * 
	 * @param pluginName
	 *            the name of the plugin
	 * @return a JavaPlugin
	 */
	public Object loadPlugin(String pluginName) {
		logger.logln("Loading plugin: " + pluginName);
		File dir = new File("./plugins");
		File f = new File(dir, pluginName + ".jar");
		if (!f.exists()) {
			logger.errorln("Could not load jar: " + f.getPath()
					+ "! The file was not found.");
			return null;
		}

		PluginInfo pi = null;
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(f);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry = entries.nextElement();
				if (!zipEntry.isDirectory()) {
					final String fileName = zipEntry.getName();
					if (fileName.endsWith("plugin.yml")) {
						InputStream input = zipFile.getInputStream(zipEntry);
						pi = PluginInfo.generatePluginInfo(input);
						break;
					}
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				zipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (pi == null) {
			logger.errorln("Could not load jar: " + pluginName
					+ "! The plugin info could not be loaded.");
			return null;
		}

		if (plugins.containsKey(pi.name)) {
			logger.errorln("Could not load jar: "
					+ pluginName
					+ "! The plugin was already loaded. Returning the loaded plugin.");
			return plugins.get(pi.name);
		}

		currentPlugin = pi.name;
		PluginInfo.pluginInfo.put(pi.name, pi);

		try {
			URL myJarFile = f.toURI().toURL();
			URLClassLoader sysLoader = (URLClassLoader) ClassLoader
					.getSystemClassLoader();
			Class<URLClassLoader> sysClass = URLClassLoader.class;
			Method sysMethod = sysClass.getDeclaredMethod("addURL",
					new Class[] { URL.class });
			sysMethod.setAccessible(true);
			sysMethod.invoke(sysLoader, new Object[] { myJarFile });

			URLClassLoader cl = URLClassLoader
					.newInstance(new URL[] { myJarFile });
			Class<?> plugin = cl.loadClass(pi.mainClass);
			Object pluginInstance = plugin.newInstance();
			try {
				Method enable = plugin.getMethod("loadPluginInfo");
				enable.invoke(pluginInstance);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				Method enable = plugin.getMethod("onEnable");
				enable.invoke(pluginInstance);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			plugins.put(pi.name, pluginInstance);
			logger.logln("Loaded plugin: " + pluginName + ".");
			return pluginInstance;
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.errorln("Could not load jar: " + pluginName + "!");

		return null;
	}

	/**
	 * Get a plugin by its name
	 * 
	 * @param pluginName
	 *            the name of the plugin
	 * @return the plugin
	 */
	public JavaPlugin getPlugin(String pluginName) {
		return (JavaPlugin) plugins.get(pluginName);
	}

	/**
	 * Unload a plugin
	 * 
	 * @param plugin
	 *            the plugin name
	 * @return true
	 */
	public static boolean unLoadPlugin(String plugin) {
		((JavaPlugin) plugins.get(plugin)).onDisable();
		plugins.remove(plugin);
		return true;
	}

	public static void unLoadAllPlugins() {
		for (Entry<String, Object> s : plugins.entrySet()) {
			try {
				Class<?> c = s.getValue().getClass();
				Method m = c.getMethod("onDisable");
				m.invoke(c);
				plugins.remove(s.getKey());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
