package bitserver.logger;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.fusesource.jansi.AnsiConsole;

import bitserver.javaplugin.PluginInfo;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * Used for printing log messages
 */
public class Logger {

	public static List<String> log = new ArrayList<String>();
	
	public static Logger noFormatLogger = new Logger("");

	/**
	 * The format
	 */
	private static String format = "[YY:MM:DD] [HH:MM:SS] [CLASS] ";
	/**
	 * The prefix
	 */
	private String prefix;
	/**
	 * The out stream
	 */
	private PrintStream out;
	/**
	 * The error stream
	 */
	private PrintStream err;

	/**
	 * Make the logger
	 * 
	 * @param prefix
	 *            the prefix to use
	 */
	protected Logger(String prefix) {
		this.prefix = prefix;
		try {
			PrintStream out = new PrintStream(AnsiConsole.out, true, "UTF-8");
			this.out = out;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			PrintStream err = new PrintStream(AnsiConsole.err, true, "UTF-8");
			this.err = err;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Clear the screen
	 */
	public void clear() {
		out.print("\u001b[2J");
		// Ansi.ansi().eraseScreen();
		// Ansi.ansi().cursor(0, 0);
	}

	/**
	 * Log the message provided
	 * 
	 * @param message
	 *            the message
	 */
	public void log(String message) {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow));
		out.print(prefix + LoggerUtil.toANSI(message) + LoggerUtil.ANSI_RESET);
		log.add(message);
	}

	/**
	 * Log the message provided on its own line
	 * 
	 * @param message
	 *            the message
	 */
	public void logln(String message) {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow));
		out.println(prefix + LoggerUtil.toANSI(message) + LoggerUtil.ANSI_RESET);
		log.add(message);
	}

	/**
	 * Log an error
	 * 
	 * @param message
	 *            the error message
	 */
	public void error(String message) {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow));
		err.print(LoggerUtil.ANSI_RED + "[ERROR]" + LoggerUtil.ANSI_RESET
				+ prefix + LoggerUtil.toANSI(message) + LoggerUtil.ANSI_RESET);
		log.add(message);
	}

	/**
	 * Log an error on its own line
	 * 
	 * @param message
	 *            the error message
	 */
	public void errorln(String message) {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow));
		err.println(prefix + LoggerUtil.toANSI(message) + LoggerUtil.ANSI_RESET);
		log.add(message);
	}

	/**
	 * Log an error from an Exception
	 * 
	 * @param message
	 *            the exception
	 * @see Exception
	 */
	public void error(Exception message) {
		for (StackTraceElement ste : message.getStackTrace()) {
			errorln(ste.toString());
		}
		// AnsiConsole.out.println(prefix + message);
	}

	/**
	 * Get the logger
	 * 
	 * @param clazz
	 *            the class
	 * @return a logger
	 */
	public static Logger getLogger(Class<?> clazz) {
		String prefix = format;
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow))
				.replace("CLASS", clazz.getSimpleName());
		return new Logger(prefix);
	}

	/**
	 * Get a logger from a PluginInfo
	 * 
	 * @param pi
	 *            a plugin info
	 * @return a logger
	 * @see PluginInfo
	 */
	public static Logger getLogger(PluginInfo pi) {
		String prefix = format;
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		prefix = prefix.replace("YY:MM:DD", ft.format(dNow))
				.replace("HH:MM:SS", ft2.format(dNow))
				.replace("CLASS", pi.name);
		return new Logger(prefix);
	}

	/**
	 * Get a logger without a prefix
	 * 
	 * @return a logger with no prefix
	 */
	public static Logger getLoggerNoPrefix() {
		return new Logger("");
	}

}