package bitserver.logger;

import bitserver.util.ChatColour;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * Used for converting ChatColours to ANSI
 * @see ChatColour
 */
public class LoggerUtil {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BOLD = "\u001B[1m";
	public static final String ANSI_ITALICS = "\u001B[2m";
	public static final String ANSI_UNDERLINE = "\u001B[4m";
	public static final String ANSI_STRIKETHROUGH = "\u001B[9m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	public static String toANSI(String input) {
		String output = input
				.replace(ChatColour.AQUA.toString(), ANSI_CYAN)
				.replace(ChatColour.BLACK.toString(), ANSI_BLACK)
				.replace(ChatColour.BLUE.toString(), ANSI_BLUE)
				.replace(ChatColour.BOLD.toString(), ANSI_BOLD)
				.replace(ChatColour.DARK_AQUA.toString(), ANSI_CYAN)
				.replace(ChatColour.DARK_BLUE.toString(), ANSI_BLUE)
				.replace(ChatColour.DARK_GREEN.toString(), ANSI_GREEN)
				.replace(ChatColour.DARK_PURPLE.toString(), ANSI_PURPLE)
				.replace(ChatColour.DARK_RED.toString(), ANSI_RED)
				.replace(ChatColour.GOLD.toString(), ANSI_YELLOW)
				.replace(ChatColour.GRAY.toString(), ANSI_WHITE)
				.replace(ChatColour.GREEN.toString(), ANSI_GREEN)
				.replace(ChatColour.ITALIC.toString(), ANSI_ITALICS)
				.replace(ChatColour.LIGHT_PURPLE.toString(), ANSI_PURPLE)
				.replace(ChatColour.MAGIC.toString(), "")
				.replace(ChatColour.RED.toString(), ANSI_RED)
				.replace(ChatColour.RESET.toString(), ANSI_RESET)
				.replace(ChatColour.STRIKETHROUGH.toString(), ANSI_STRIKETHROUGH)
				.replace(ChatColour.UNDERLINE.toString(), ANSI_UNDERLINE)
				.replace(ChatColour.WHITE.toString(), ANSI_WHITE)
				.replace(ChatColour.YELLOW.toString(), ANSI_YELLOW);
		return output;
	}

}