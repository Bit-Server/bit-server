package bitserver.nms;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public enum Classes {
	
	EnumChatFormatting("a"),
	ICommand("aa"),
	ContainerBrewingStand$Ingredient("aaa"),
	ContainerBrewingStand$Potion("aab"),
	ICrafting("aac"),
	ContainerChest("aad"),
	InventoryCrafting("aae"),
	ContainerWorkbench("aaf"),
	ContainerEnchantment("aag"),
	ContainerEnchantment$1("aah"),
	ContainerEnchantment$2("aai"),
	ContainerFurnace("aaj"),
	SlotFurnace("aak"),
	ContainerHopper("aal"),
	ContainerHorseInventory("aam"),
	ContainerHorseInventory$1("aan"),
	ContainerHorseInventory$2("aao"),
	ContainerPlayer("aap"),
	ContainerPlayer$1("aaq"),
	InventoryMerchant("aas"),
	ContainerMerchant("aat"),
	SlotMerchantResult("aau"),
	InventoryEnderChest("aav"),
	InventoryCraftResult("aaw"),
	SlotCrafting("aax"),
	Slot("aay"),
	ContainerDispenser("aaz"),
	ICommandManager("ab"),
	ItemAnvilBlock("aba"),
	ItemArmor("abb"),
	ItemArmor$1("abc"),
	ItemArmor$ArmorMaterial("abd"),
	ItemBlockWithMetadata("abe"),
	ItemAxe("abf"),
	ItemBed("abg"),
	ItemBlock("abh"),
	ItemReed("abi"),
	ItemBoat("abj"),
	ItemBook("abk"),
	ItemGlassBottle("abl"),
	ItemBow("abm"),
	ItemSoup("abn"),
	ItemBucket("abo"),
	ItemCarrotOnAStick("abp"),
	ItemCoal("abq"),
	ItemColored("abr"),
	ItemMapBase("abs"),
	CreativeTabs("abt"),
	CreativeTabs$1("abu"),
	CreativeTabs$10("abv"),
	CreativeTabs$11("abw"),
	CreativeTabs$12("abx"),
	CreativeTabs$2("aby"),
	CreativeTabs$3("abz"),
	ICommandSender("ac"),
	CreativeTabs$4("aca"),
	CreativeTabs$5("acb"),
	CreativeTabs$6("acc"),
	CreativeTabs$7("acd"),
	CreativeTabs$8("ace"),
	CreativeTabs$9("acf"),
	ItemTool("acg"),
	ItemDoor("ach"),
	ItemDoublePlant("aci"),
	ItemDye("acj"),
	ItemEgg("ack"),
	ItemEmptyMap("acl"),
	ItemEnchantedBook("acm"),
	ItemEnderEye("acn"),
	ItemEnderPearl("aco"),
	ItemExpBottle("acp"),
	ItemFireball("acq"),
	ItemFireworkCharge("acr"),
	ItemFirework("acs"),
	ItemFishFood("act"),
	ItemFishFood$FishType("acu"),
	ItemFishingRod("acv"),
	ItemFlintAndSteel("acw"),
	ItemFood("acx"),
	ItemAppleGold("acy"),
	ItemHangingEntity("acz"),
	PlayerSelector("ad"),
	ItemHoe("ada"),
	Item("adb"),
	Item$ToolMaterial("adc"),
	ItemStack("add"),
	Items("ade"),
	ItemLead("adf"),
	ItemLeaves("adg"),
	ItemMap("adh"),
	ItemBucketMilk("adi"),
	ItemMinecart("adj"),
	ItemMinecart$1("adk"),
	ItemMultiTexture("adl"),
	ItemNameTag("adm"),
	ItemPickaxe("adn"),
	ItemPiston("ado"),
	ItemPotion("adp"),
	EnumRarity("adq"),
	ItemRecord("adr"),
	ItemRedstone("ads"),
	ItemSaddle("adt"),
	ItemSeedFood("adv"),
	ItemSeeds("adw"),
	ItemShears("adx"),
	ItemSpade("ady"),
	ItemSign("adz"),
	CommandAchievement("ae"),
	ItemSimpleFoiled("aea"),
	ItemSkull("aeb"),
	ItemSnow("aec"),
	ItemSnowball("aed"),
	ItemMonsterPlacer("aee"),
	ItemSlab("aeg"),
	ItemSword("aeh"),
	EnumAction("aei"),
	ItemLilyPad("aej"),
	ItemCloth("aek"),
	ItemWritableBook("ael"),
	ItemEditableBook("aem"),
	PotionHelper("aen"),
	RecipesArmorDyes("aeu"),
	RecipesArmor("aev"),
	RecipeBookCloning("aew"),
	RecipesDyes("aex"),
	RecipeFireworks("aey"),
	RecipesFood("aez"),
	CommandBanIp("af"),
	FurnaceRecipes("afa"),
	RecipesMapCloning("afb"),
	RecipesMapExtending("afc"),
	RecipesIngots("afd"),
	CraftingManager("afe"),
	CraftingManager$1("aff"),
	IRecipe("afg"),
	ShapedRecipes("afh"),
	ShapelessRecipes("afi"),
	RecipesCrafting("afj"),
	RecipesTools("afk"),
	RecipesWeapons("afl"),
	EnchantmentArrowDamage("afm"),
	EnchantmentArrowFire("afn"),
	EnchantmentArrowInfinite("afo"),
	EnchantmentArrowKnockback("afp"),
	EnchantmentDamage("afq"),
	EnchantmentDurability("afr"),
	EnchantmentDigging("afs"),
	Enchantment("aft"),
	EnumEnchantmentType("afu"),
	EnchantmentHelper("afv"),
	EnchantmentHelper$1("afw"),
	EnchantmentHelper$DamageIterator("afx"),
	EnchantmentHelper$HurtIterator("afy"),
	EnchantmentHelper$IModifier("afz"),
	CommandBanPlayer("ag"),
	EnchantmentHelper$ModifierLiving("aga"),
	EnchantmentHelper$ModifierDamage("agb"),
	EnchantmentData("agc"),
	EnchantmentFireAspect("agd"),
	EnchantmentFishingSpeed("age"),
	EnchantmentKnockback("agf"),
	EnchantmentLootBonus("agg"),
	EnchantmentOxygen("agh"),
	EnchantmentProtection("agi"),
	EnchantmentThorns("agj"),
	EnchantmentUntouching("agk"),
	EnchantmentWaterWorker("agl"),
	IMerchant("agm"),
	MerchantRecipe("agn"),
	MerchantRecipeList("ago"),
	CommandBlockLogic("agp"),
	MobSpawnerBaseLogic("agq"),
	MobSpawnerBaseLogic$WeightedRandomMinecart("agr"),
	BlockEventData("ags"),
	ChunkPosition("agt"),
	ChunkCoordIntPair("agu"),
	Explosion("agw"),
	ColorizerFoliage("agx"),
	GameRules("agy"),
	GameRules$Value("agz"),
	CommandBroadcast("ah"),
	ColorizerGrass("aha"),
	World("ahb"),
	World$1("ahc"),
	World$2("ahd"),
	World$3("ahe"),
	World$4("ahf"),
	MinecraftException("ahg"),
	IWorldAccess("ahh"),
	WorldSettings("ahj"),
	WorldSettings$GameType("ahk"),
	IBlockAccess("ahl"),
	WorldType("ahm"),
	EnumSkyBlock("ahn"),
	SpawnerAnimals("aho"),
	Teleporter("ahp"),
	Teleporter$PortalPosition("ahq"),
	ChunkCache("ahr"),
	NextTickListEntry("ahs"),
	BiomeGenBeach("aht"),
	BiomeGenBase("ahu"),
	BiomeGenBase$Height("ahv"),
	BiomeGenBase$TempCategory("ahw"),
	BiomeGenBase$SpawnListEntry("ahx"),
	BiomeCache("ahy"),
	BiomeCache$Block("ahz"),
	CommandClearInventory("ai"),
	BiomeDecorator("aia"),
	WorldChunkManager("aib"),
	BiomeGenDesert("aic"),
	BiomeGenHills("aid"),
	WorldChunkManagerHell("aie"),
	BiomeGenForest("aif"),
	BiomeGenForest$1("aig"),
	BiomeGenForest$2("aih"),
	BiomeGenHell("aii"),
	BiomeGenSnow("aij"),
	BiomeGenJungle("aik"),
	BiomeGenMesa("ail"),
	BiomeGenMushroomIsland("aim"),
	BiomeGenMutated("ain"),
	BiomeGenOcean("aio"),
	BiomeGenPlains("aip"),
	BiomeGenRiver("air"),
	BiomeGenSavanna("ais"),
	BiomeGenSavanna$Mutated("ait"),
	BiomeGenStoneBeach("aiu"),
	BiomeGenSwamp("aiv"),
	BiomeGenTaiga("aiw"),
	BiomeGenEnd("aix"),
	BiomeEndDecorator("aiy"),
	WorldGenWaterlily("aiz"),
	CommandDeOp("aj"),
	BlockAir("aja"),
	BlockAnvil("ajb"),
	BlockContainer("ajc"),
	BlockBasePressurePlate("ajd"),
	BlockRailBase("aje"),
	BlockRailBase$Rail("ajf"),
	BlockBeacon("ajg"),
	BlockBed("ajh"),
	Block("aji"),
	Block$1("ajj"),
	Block$2("ajk"),
	Block$3("ajl"),
	Block$SoundType("ajm"),
	Blocks("ajn"),
	IGrowable("ajo"),
	BlockBookshelf("ajp"),
	BlockBrewingStand("ajq"),
	BlockBush("ajr"),
	BlockButton("ajs"),
	BlockCactus("ajt"),
	BlockCake("aju"),
	BlockCarrot("ajv"),
	BlockCauldron("ajw"),
	BlockChest("ajx"),
	BlockClay("ajy"),
	BlockCocoa("ajz"),
	CommandDebug("ak"),
	BlockColored("aka"),
	BlockCommandBlock("akb"),
	BlockRedstoneComparator("akc"),
	BlockWorkbench("ake"),
	BlockCrops("akf"),
	BlockDaylightDetector("akg"),
	BlockDeadBush("akh"),
	BlockRailDetector("aki"),
	BlockRedstoneDiode("akj"),
	BlockDirectional("akk"),
	BlockDirt("akl"),
	BlockDispenser("akm"),
	BlockDoor("akn"),
	BlockDoublePlant("ako"),
	BlockDragonEgg("akp"),
	BlockDropper("akq"),
	BlockDynamicLiquid("akr"),
	BlockEnchantmentTable("aks"),
	BlockEndPortal("akt"),
	BlockEndPortalFrame("aku"),
	BlockEnderChest("akv"),
	ITileEntityProvider("akw"),
	BlockFalling("akx"),
	BlockFarmland("aky"),
	BlockFence("akz"),
	CommandDefaultGameMode("al"),
	BlockFenceGate("ala"),
	BlockFire("alb"),
	BlockFlower("alc"),
	BlockFlowerPot("ald"),
	BlockFurnace("ale"),
	BlockGlass("alf"),
	BlockGlowstone("alg"),
	BlockGrass("alh"),
	BlockGravel("ali"),
	BlockSlab("alj"),
	BlockBreakable("alk"),
	BlockHardenedClay("all"),
	BlockHay("alm"),
	BlockHopper("aln"),
	BlockHugeMushroom("alo"),
	BlockIce("alp"),
	BlockJukebox("alq"),
	BlockJukebox$TileEntityJukebox("alr"),
	BlockLadder("als"),
	BlockLeaves("alt"),
	BlockLever("alv"),
	BlockLiquid("alw"),
	BlockLog("alx"),
	BlockMelon("aly"),
	BlockCompressed("alz"),
	CommandEffect("am"),
	BlockMobSpawner("ama"),
	BlockSilverfish("amb"),
	BlockMushroom("amc"),
	BlockMycelium("amd"),
	BlockNetherWart("amf"),
	BlockNetherrack("amg"),
	BlockNewLeaf("amh"),
	BlockNewLog("ami"),
	BlockNote("amj"),
	BlockObsidian("amk"),
	BlockOldLeaf("aml"),
	BlockOldLog("amm"),
	BlockOre("amn"),
	BlockPackedIce("amo"),
	BlockPortal("amp"),
	BlockPortal$Size("amq"),
	BlockPotato("amr"),
	BlockCompressedPowered("ams"),
	BlockRailPowered("amt"),
	BlockPressurePlate("amu"),
	BlockPressurePlate$Sensitivity("amv"),
	BlockPumpkin("amw"),
	BlockQuartz("amx"),
	BlockRail("amy"),
	BlockRedstoneOre("amz"),
	CommandEmote("an"),
	BlockRedstoneWire("ana"),
	BlockRedstoneLight("anb"),
	BlockRedstoneTorch("anc"),
	BlockRedstoneTorch$Toggle("and"),
	BlockReed("ane"),
	BlockRedstoneRepeater("anf"),
	BlockRotatedPillar("ang"),
	BlockSand("anh"),
	BlockSandStone("ani"),
	BlockSapling("anj"),
	BlockSign("ank"),
	BlockSkull("anl"),
	BlockSnowBlock("anm"),
	BlockSnow("ann"),
	BlockSoulSand("ano"),
	BlockSponge("anp"),
	BlockStainedGlass("anq"),
	BlockStainedGlassPane("anr"),
	BlockStairs("ans"),
	BlockStaticLiquid("ant"),
	BlockStem("anu"),
	BlockStone("anv"),
	BlockStoneBrick("anw"),
	BlockButtonStone("anx"),
	BlockStoneSlab("any"),
	BlockTallGrass("anz"),
	CommandEnchant("ao"),
	BlockPane("aoa"),
	BlockTNT("aob"),
	BlockTorch("aoc"),
	BlockLeavesBase("aod"),
	BlockTrapDoor("aoe"),
	BlockTripWire("aof"),
	BlockTripWireHook("aog"),
	BlockVine("aoh"),
	BlockWall("aoi"),
	BlockLilyPad("aoj"),
	BlockWeb("aok"),
	BlockPressurePlateWeighted("aol"),
	BlockWood("aom"),
	BlockButtonWood("aon"),
	BlockWoodSlab("aoo"),
	BlockCarpet("aop"),
	TileEntityBeacon("aoq"),
	TileEntity("aor"),
	TileEntity$1("aos"),
	TileEntity$2("aot"),
	TileEntity$3("aou"),
	TileEntityBrewingStand("aov"),
	TileEntityChest("aow"),
	TileEntityCommandBlock("aox"),
	TileEntityCommandBlock$1("aoy"),
	TileEntityComparator("aoz"),
	CommandXP("ap"),
	TileEntityDaylightDetector("apa"),
	TileEntityDispenser("apb"),
	TileEntityDropper("apc"),
	TileEntityEnchantmentTable("apd"),
	TileEntityEnderChest("ape"),
	TileEntityFlowerPot("apf"),
	TileEntityFurnace("apg"),
	IHopper("aph"),
	TileEntityHopper("api"),
	TileEntityMobSpawner("apj"),
	TileEntityMobSpawner$1("apk"),
	TileEntityNote("apl"),
	TileEntitySign("apm"),
	TileEntitySkull("apn"),
	TileEntityEndPortal("apo"),
	BlockPistonBase("app"),
	BlockPistonExtension("apq"),
	BlockPistonMoving("apr"),
	TileEntityPiston("aps"),
	IChunkProvider("apu"),
	NibbleArray("apv"),
	EmptyChunk("apw"),
	Chunk("apx"),
	Chunk$1("apy"),
	ExtendedBlockStorage("apz"),
	CommandDifficulty("aq"),
	NibbleArrayReader("aqa"),
	IChunkLoader("aqc"),
	ChunkLoader("aqf"),
	ChunkLoader$AnvilConverterData("aqg"),
	RegionFile("aqh"),
	RegionFile$ChunkBuffer("aqi"),
	RegionFileCache("aqj"),
	AnvilChunkLoader("aqk"),
	AnvilChunkLoader$PendingChunk("aql"),
	WorldProvider("aqo"),
	WorldProviderHell("aqp"),
	WorldProviderSurface("aqq"),
	WorldProviderEnd("aqr"),
	MapGenRavine("aqs"),
	ChunkProviderFlat("aqu"),
	ChunkProviderHell("aqv"),
	MapGenCaves("aqw"),
	MapGenBase("aqx"),
	MapGenCavesHell("aqy"),
	ChunkProviderGenerate("aqz"),
	CommandGameMode("ar"),
	ChunkProviderEnd("ara"),
	WorldGenAbstractTree("arc"),
	WorldGenBigTree("ard"),
	WorldGenForest("are"),
	WorldGenBlockBlob("arf"),
	WorldGeneratorBonusChest("arg"),
	WorldGenCactus("arh"),
	WorldGenClay("arj"),
	WorldGenDeadBush("ark"),
	WorldGenDesertWells("arl"),
	WorldGenDoublePlant("arm"),
	WorldGenerator("arn"),
	WorldGenFlowers("aro"),
	WorldGenShrub("arp"),
	WorldGenFire("arq"),
	WorldGenGlowStone2("arr"),
	WorldGenHellLava("ars"),
	WorldGenBigMushroom("aru"),
	WorldGenIcePath("arv"),
	WorldGenIceSpike("arw"),
	WorldGenLakes("arx"),
	WorldGenGlowStone1("ary"),
	WorldGenMegaJungle("arz"),
	CommandGameRule("as"),
	WorldGenMegaPineTree("asa"),
	WorldGenHugeTrees("asb"),
	WorldGenMelon("asc"),
	WorldGenDungeons("asd"),
	WorldGenMinable("ase"),
	WorldGenTaiga1("asf"),
	WorldGenPumpkin("asg"),
	WorldGenReed("ash"),
	WorldGenCanopyTree("asi"),
	WorldGenSand("asj"),
	WorldGenSavannaTree("ask"),
	WorldGenSpikes("asl"),
	WorldGenLiquids("asm"),
	WorldGenTaiga2("asn"),
	WorldGenSwamp("aso"),
	WorldGenTallGrass("asp"),
	WorldGenTrees("asq"),
	WorldGenVines("asr"),
	FlatGeneratorInfo("ass"),
	FlatLayerInfo("ast"),
	StructureBoundingBox("asv"),
	MapGenMineshaft("asw"),
	StructureMineshaftPieces("asx"),
	StructureMineshaftPieces$Corridor("asy"),
	StructureMineshaftPieces$Cross("asz"),
	CommandGive("at"),
	StructureMineshaftPieces$Room("ata"),
	StructureMineshaftPieces$Stairs("atb"),
	StructureMineshaftStart("atc"),
	MapGenNetherBridge("atd"),
	MapGenNetherBridge$Start("ate"),
	StructureNetherBridgePieces("atf"),
	StructureNetherBridgePieces$Crossing3("atg"),
	StructureNetherBridgePieces$End("ath"),
	StructureNetherBridgePieces$Straight("ati"),
	StructureNetherBridgePieces$Corridor3("atj"),
	StructureNetherBridgePieces$Corridor4("atk"),
	StructureNetherBridgePieces$Entrance("atl"),
	StructureNetherBridgePieces$Crossing2("atm"),
	StructureNetherBridgePieces$Corridor("atn"),
	StructureNetherBridgePieces$Corridor5("ato"),
	StructureNetherBridgePieces$Corridor2("atp"),
	StructureNetherBridgePieces$NetherStalkRoom("atq"),
	StructureNetherBridgePieces$Throne("atr"),
	StructureNetherBridgePieces$Piece("ats"),
	StructureNetherBridgePieces$PieceWeight("att"),
	StructureNetherBridgePieces$Crossing("atu"),
	StructureNetherBridgePieces$Stairs("atv"),
	StructureNetherBridgePieces$Start("atw"),
	MapGenScatteredFeature("atx"),
	MapGenScatteredFeature$Start("aty"),
	ComponentScatteredFeaturePieces("atz"),
	CommandHelp("au"),
	ComponentScatteredFeaturePieces$1("aua"),
	ComponentScatteredFeaturePieces$DesertPyramid("aub"),
	ComponentScatteredFeaturePieces$JunglePyramid("auc"),
	ComponentScatteredFeaturePieces$JunglePyramid$Stones("aud"),
	ComponentScatteredFeaturePieces$Feature("aue"),
	ComponentScatteredFeaturePieces$SwampHut("auf"),
	MapGenStronghold("aug"),
	MapGenStronghold$Start("auh"),
	StructureStrongholdPieces("aui"),
	StructureStrongholdPieces$1("auj"),
	StructureStrongholdPieces$2("auk"),
	StructureStrongholdPieces$SwitchDoor("aul"),
	StructureStrongholdPieces$ChestCorridor("aum"),
	StructureStrongholdPieces$Corridor("aun"),
	StructureStrongholdPieces$Crossing("auo"),
	StructureStrongholdPieces$LeftTurn("aup"),
	StructureStrongholdPieces$Library("auq"),
	StructureStrongholdPieces$PieceWeight("aur"),
	StructureStrongholdPieces$PortalRoom("aus"),
	StructureStrongholdPieces$Prison("aut"),
	StructureStrongholdPieces$RightTurn("auu"),
	StructureStrongholdPieces$RoomCrossing("auv"),
	StructureStrongholdPieces$Stones("auw"),
	StructureStrongholdPieces$Stairs("auy"),
	StructureStrongholdPieces$Stairs2("auz"),
	CommandServerKick("av"),
	StructureStrongholdPieces$Straight("ava"),
	StructureStrongholdPieces$StairsStraight("avb"),
	StructureStrongholdPieces$Stronghold("avc"),
	StructureStrongholdPieces$Stronghold$Door("avd"),
	MapGenStructure("ave"),
	MapGenStructure$1("avf"),
	MapGenStructure$2("avg"),
	MapGenStructure$3("avh"),
	MapGenStructureIO("avi"),
	MapGenStructureData("avj"),
	StructureComponent("avk"),
	StructureComponent$BlockSelector("avl"),
	StructureStart("avm"),
	MapGenVillage("avn"),
	MapGenVillage$Start("avo"),
	StructureVillagePieces("avp"),
	StructureVillagePieces$House1("avq"),
	StructureVillagePieces$Field1("avr"),
	StructureVillagePieces$Field2("avs"),
	StructureVillagePieces$Torch("avt"),
	StructureVillagePieces$PieceWeight("avu"),
	StructureVillagePieces$Hall("avv"),
	StructureVillagePieces$House4Garden("avw"),
	StructureVillagePieces$WoodHut("avx"),
	StructureVillagePieces$Church("avy"),
	StructureVillagePieces$House2("avz"),
	CommandKill("aw"),
	StructureVillagePieces$Start("awa"),
	StructureVillagePieces$Path("awb"),
	StructureVillagePieces$House3("awc"),
	StructureVillagePieces$Village("awd"),
	StructureVillagePieces$Road("awe"),
	StructureVillagePieces$Well("awf"),
	NoiseGeneratorImproved("awj"),
	NoiseGeneratorOctaves("awk"),
	NoiseGeneratorPerlin("awl"),
	NoiseGeneratorSimplex("awo"),
	NoiseGenerator("awp"),
	MaterialLogic("awq"),
	MaterialTransparent("awr"),
	MaterialLiquid("aws"),
	Material("awt"),
	Material$1("awu"),
	MapColor("awv"),
	MaterialPortal("aww"),
	GenLayerDeepOcean("awx"),
	GenLayerEdge("awy"),
	GenLayerEdge$SwitchMode("awz"),
	CommandListBans("ax"),
	GenLayerEdge$Mode("axa"),
	GenLayerAddIsland("axb"),
	GenLayerAddMushroomIsland("axc"),
	GenLayerAddSnow("axd"),
	GenLayerBiomeEdge("axe"),
	GenLayerBiome("axf"),
	GenLayerFuzzyZoom("axj"),
	IntCache("axl"),
	GenLayerIsland("axm"),
	GenLayer("axn"),
	GenLayer$1("axo"),
	GenLayer$2("axp"),
	GenLayerRareBiome("axq"),
	GenLayerHills("axr"),
	GenLayerRemoveTooMuchOcean("axs"),
	GenLayerRiverInit("axt"),
	GenLayerRiver("axu"),
	GenLayerRiverMix("axv"),
	GenLayerShore("axw"),
	GenLayerSmooth("axx"),
	CommandListPlayers("ay"),
	GenLayerVoronoiZoom("ayb"),
	GenLayerZoom("ayc"),
	Path("ayd"),
	PathPoint("aye"),
	PathEntity("ayf"),
	PathFinder("ayg"),
	MapData("ayi"),
	MapData$MapInfo("ayj"),
	MapData$MapCoord("ayk"),
	WorldSavedData("ayl"),
	AnvilSaveHandler("aym"),
	AnvilSaveConverter("ayn"),
	AnvilSaveConverter$1("ayo"),
	DerivedWorldInfo("ayp"),
	SaveHandler("ayq"),
	SaveFormatOld("ayr"),
	WorldInfo("ays"),
	WorldInfo$1("ayt"),
	WorldInfo$2("ayu"),
	WorldInfo$3("ayv"),
	WorldInfo$4("ayw"),
	WorldInfo$5("ayx"),
	WorldInfo$6("ayy"),
	WorldInfo$7("ayz"),
	CommandMessage("az"),
	WorldInfo$8("aza"),
	WorldInfo$9("azb"),
	ISaveHandler("azc"),
	ISaveFormat("aze"),
	SaveFormatComparator("azf"),
	SaveHandlerMP("azo"),
	IPlayerFileData("azp"),
	MapStorage("azq"),
	ThreadedFileIOBase("azr"),
	IThreadedFileIO("azs"),
	AxisAlignedBB("azt"),
	MovingObjectPosition("azu"),
	MovingObjectPosition$MovingObjectType("azv"),
	Vec3("azw"),
	ScoreObjective("azx"),
	ScorePlayerTeam("azy"),
	Score("azz"),
	CrashReport("b"),
	CommandMessageRaw("ba"),
	Score$1("baa"),
	Scoreboard("bac"),
	ScoreboardSaveData("bad"),
	Team("bae"),
	ScoreDummyCriteria("baf"),
	ScoreHealthCriteria("bag"),
	IScoreObjectiveCriteria("bah"),
	ObjectiveStat("bai"),
	CommandNetstat("bb"),
	MouseHelper("bbg"),
	ScreenShotHelper("bbp"),
	MinecraftError("bbq"),
	Timer("bbr"),
	Session("bbs"),
	Session$Type("bbt"),
	CommandOp("bc"),
	CommandPardonIp("bd"),
	CommandPardonPlayer("be"),
	CommandPlaySound("bf"),
	EnchantmentNameParts("bfp"),
	CommandPublishLocalServer("bg"),
	CommandSaveAll("bh"),
	CommandSaveOff("bi"),
	CommandSaveOn("bj"),
	CommandScoreboard("bk"),
	ServerCommandManager("bl"),
	MovementInput("bli"),
	MovementInputFromOptions("blj"),
	BossStatus("bln"),
	CommandSetBlock("bm"),
	CommandSetDefaultSpawnpoint("bn"),
	CommandSetPlayerTimeout("bo"),
	CommandSetSpawnpoint("bp"),
	CommandShowSeed("bq"),
	ResourceLocation("bqx"),
	CommandSpreadPlayers("br"),
	CommandSpreadPlayers$Position("bs"),
	IntegratedPlayerList("bsw"),
	IntegratedServer("bsx"),
	IntegratedServer$1("bsy"),
	IntegratedServer$2("bsz"),
	CommandStop("bt"),
	CommandSummon("bu"),
	RealmsConnect$1("bus"),
	RealmsServerStatusPinger$1("but"),
	CommandTeleport("bv"),
	CommandTestForBlock("bx"),
	CommandTestFor("by"),
	CommandTime("bz"),
	CrashReport$1("c"),
	CommandToggleDownfall("ca"),
	CommandWeather("cb"),
	CommandWhitelist("cc"),
	CommandException("cd"),
	NumberInvalidException("ce"),
	SyntaxErrorException("cf"),
	PlayerNotFoundException("cg"),
	CommandNotFoundException("ch"),
	WrongUsageException("ci"),
	IBlockSource("ck"),
	BlockSourceImpl("cl"),
	BehaviorDefaultDispenseItem("cm"),
	RegistryNamespacedDefaultedByKey("cn"),
	RegistryDefaulted("co"),
	IBehaviorDispenseItem("cp"),
	IBehaviorDispenseItem$1("cq"),
	EnumFacing("cr"),
	IObjectIntIterable("cs"),
	ObjectIntIdentityMap("ct"),
	ILocatableSource("cu"),
	ILocation("cv"),
	RegistryNamespaced("cw"),
	IPosition("cx"),
	PositionImpl("cy"),
	IRegistry("cz"),
	CrashReport$2("d"),
	RegistrySimple("da"),
	StatCollector("dd"),
	StringTranslate("de"),
	NBTTagByteArray("df"),
	NBTTagByte("dg"),
	NBTTagCompound("dh"),
	NBTTagCompound$1("di"),
	NBTTagCompound$2("dj"),
	NBTTagDouble("dk"),
	NBTTagEnd("dl"),
	NBTTagFloat("dm"),
	NBTTagIntArray("dn"),
	NBTTagInt("dp"),
	NBTTagList("dq"),
	NBTTagLong("dr"),
	NBTSizeTracker("ds"),
	NBTSizeTracker$1("dt"),
	CompressedStreamTools("du"),
	NBTUtil("dv"),
	NBTTagShort("dw"),
	NBTTagString("dx"),
	NBTBase("dy"),
	NBTBase$NBTPrimitive("dz"),
	CrashReport$3("e"),
	NBTException("ea"),
	JsonToNBT("eb"),
	JsonToNBT$Any("ec"),
	JsonToNBT$Compound("ed"),
	JsonToNBT$List("ee"),
	JsonToNBT$Primitive("ef"),
	NettyEncryptionTranslator("eg"),
	NettyEncryptingDecoder("eh"),
	NettyEncryptingEncoder("ei"),
	NetworkManager("ej"),
	NetworkManager$1("ek"),
	NetworkManager$2("el"),
	NetworkManager$3("em"),
	NetworkManager$InboundHandlerTuplePacketListener("en"),
	EnumConnectionState("eo"),
	EnumConnectionState$2("ep"),
	EnumConnectionState$4("eq"),
	EnumConnectionState$3("er"),
	EnumConnectionState$1("es"),
	PacketBuffer("et"),
	NetworkStatistics("eu"),
	NetworkStatistics$1("ev"),
	NetworkStatistics$PacketStat("ew"),
	NetworkStatistics$Tracker("ex"),
	NetworkStatistics$PacketStatData("ey"),
	MessageDeserializer("ez"),
	CrashReport$4("f"),
	MessageSerializer("fa"),
	INetHandler("fb"),
	MessageDeserializer2("fc"),
	MessageSerializer2("fd"),
	ChatComponentStyle("fe"),
	ChatComponentStyle$1("ff"),
	ChatComponentStyle$2("fg"),
	ClickEvent("fh"),
	ClickEvent$Action("fi"),
	IChatComponent("fj"),
	IChatComponent$Serializer("fk"),
	HoverEvent("fl"),
	HoverEvent$Action("fm"),
	ChatStyle("fn"),
	ChatStyle$1("fo"),
	ChatStyle$Serializer("fp"),
	ChatComponentText("fq"),
	ChatComponentTranslation("fr"),
	ChatComponentTranslationFormatException("fs"),
	Packet("ft"),
	S08PacketPlayerPosLook("fu"),
	INetHandlerPlayClient("fv"),
	S0EPacketSpawnObject("fw"),
	S11PacketSpawnExperienceOrb("fx"),
	S2CPacketSpawnGlobalEntity("fy"),
	S0FPacketSpawnMob("fz"),
	CrashReport$5("g"),
	S10PacketSpawnPainting("ga"),
	S0CPacketSpawnPlayer("gb"),
	S0BPacketAnimation("gc"),
	S37PacketStatistics("gd"),
	S25PacketBlockBreakAnim("ge"),
	S35PacketUpdateTileEntity("gf"),
	S24PacketBlockAction("gg"),
	S23PacketBlockChange("gh"),
	S3APacketTabComplete("gi"),
	S02PacketChat("gj"),
	S22PacketMultiBlockChange("gk"),
	S32PacketConfirmTransaction("gl"),
	S2EPacketCloseWindow("gm"),
	S2DPacketOpenWindow("gn"),
	S30PacketWindowItems("go"),
	S31PacketWindowProperty("gp"),
	S2FPacketSetSlot("gq"),
	S3FPacketCustomPayload("gr"),
	S40PacketDisconnect("gs"),
	S19PacketEntityStatus("gt"),
	S27PacketExplosion("gu"),
	S2BPacketChangeGameState("gv"),
	S00PacketKeepAlive("gw"),
	S21PacketChunkData("gx"),
	S21PacketChunkData$Extracted("gy"),
	S26PacketMapChunkBulk("gz"),
	CrashReport$6("h"),
	S28PacketEffect("ha"),
	S2APacketParticles("hb"),
	S29PacketSoundEffect("hc"),
	S01PacketJoinGame("hd"),
	S34PacketMaps("he"),
	S14PacketEntity("hf"),
	S14PacketEntity$S15PacketEntityRelMove("hg"),
	S14PacketEntity$S17PacketEntityLookMove("hh"),
	S14PacketEntity$S16PacketEntityLook("hi"),
	S36PacketSignEditorOpen("hj"),
	S39PacketPlayerAbilities("hk"),
	S38PacketPlayerListItem("ho"),
	S0APacketUseBed("hp"),
	S13PacketDestroyEntities("hq"),
	S1EPacketRemoveEntityEffect("hr"),
	S07PacketRespawn("hs"),
	S19PacketEntityHeadLook("ht"),
	S09PacketHeldItemChange("hu"),
	S3DPacketDisplayScoreboard("hv"),
	S1CPacketEntityMetadata("hw"),
	S1BPacketEntityAttach("hx"),
	S12PacketEntityVelocity("hy"),
	S04PacketEntityEquipment("hz"),
	CrashReport$7("i"),
	S1FPacketSetExperience("ia"),
	S06PacketUpdateHealth("ib"),
	S3BPacketScoreboardObjective("ic"),
	S3EPacketTeams("id"),
	S3CPacketUpdateScore("ie"),
	S05PacketSpawnPosition("ig"),
	S03PacketTimeUpdate("ih"),
	S33PacketUpdateSign("ii"),
	S0DPacketCollectItem("ij"),
	S18PacketEntityTeleport("ik"),
	S20PacketEntityProperties("il"),
	S20PacketEntityProperties$Snapshot("im"),
	S1DPacketEntityEffect("in"),
	INetHandlerPlayServer("io"),
	C0APacketAnimation("ip"),
	C14PacketTabComplete("iq"),
	C01PacketChatMessage("ir"),
	C16PacketClientStatus("is"),
	C16PacketClientStatus$EnumState("it"),
	C15PacketClientSettings("iu"),
	C0FPacketConfirmTransaction("iv"),
	C11PacketEnchantItem("iw"),
	C0EPacketClickWindow("ix"),
	C0DPacketCloseWindow("iy"),
	C17PacketCustomPayload("iz"),
	CrashReport$8("j"),
	C02PacketUseEntity("ja"),
	C02PacketUseEntity$Action("jb"),
	C00PacketKeepAlive("jc"),
	C03PacketPlayer("jd"),
	C03PacketPlayer$C04PacketPlayerPosition("je"),
	C03PacketPlayer$C06PacketPlayerPosLook("jf"),
	C03PacketPlayer$C05PacketPlayerLook("jg"),
	C13PacketPlayerAbilities("jh"),
	C07PacketPlayerDigging("ji"),
	C0BPacketEntityAction("jj"),
	C0CPacketInput("jk"),
	C09PacketHeldItemChange("jl"),
	C10PacketCreativeInventoryAction("jm"),
	C12PacketUpdateSign("jn"),
	C08PacketPlayerBlockPlacement("jo"),
	C00Handshake("jp"),
	INetHandlerHandshakeServer("jq"),
	INetHandlerLoginClient("jr"),
	S02PacketLoginSuccess("js"),
	S01PacketEncryptionRequest("jt"),
	S00PacketDisconnect("ju"),
	INetHandlerLoginServer("jv"),
	C00PacketLoginStart("jw"),
	C01PacketEncryptionResponse("jx"),
	INetHandlerStatusClient("jy"),
	S01PacketPong("jz"),
	CrashReportCategory("k"),
	S00PacketServerInfo("ka"),
	ServerStatusResponse("kb"),
	ServerStatusResponse$PlayerCountData("kc"),
	ServerStatusResponse$PlayerCountData$Serializer("kd"),
	ServerStatusResponse$Serializer("ke"),
	ServerStatusResponse$MinecraftProtocolVersionIdentifier("kf"),
	ServerStatusResponse$MinecraftProtocolVersionIdentifier$Serializer("kg"),
	INetHandlerStatusServer("kh"),
	C01PacketPing("ki"),
	C00PacketServerQuery("kj"),
	BehaviorProjectileDispense("kk"),
	Bootstrap("kl"),
	Bootstrap$1("km"),
	Bootstrap$10("kn"),
	Bootstrap$11("ko"),
	Bootstrap$12("kp"),
	Bootstrap$13("kq"),
	Bootstrap$14("kr"),
	Bootstrap$2("ks"),
	Bootstrap$3("kt"),
	Bootstrap$4("ku"),
	Bootstrap$5("kv"),
	Bootstrap$5$1("kw"),
	Bootstrap$6("kx"),
	Bootstrap$7("ky"),
	Bootstrap$8("kz"),
	CrashReportCategory$1("l"),
	Bootstrap$9("la"),
	ServerCommand("le"),
	ServerEula("lg"),
	MinecraftServer$1("lh"),
	MinecraftServer$2("li"),
	MinecraftServer$3("lj"),
	MinecraftServer$4("lk"),
	MinecraftServer$5("ll"),
	MinecraftServer$6("lm"),
	PlayerPositionComparator("ln"),
	IServer("lo"),
	ServerScoreboard("lp"),
	PropertyManager("lq"),
	IUpdatePlayerListBox("lr"),
	DedicatedPlayerList("ls"),
	DedicatedServer("lt"),
	DedicatedServer$1("lu"),
	DedicatedServer$2("lv"),
	DedicatedServer$3("lw"),
	DedicatedServer$4("lx"),
	MinecraftServerGui("ly"),
	MinecraftServerGui$1("lz"),
	CrashReportCategory$2("m"),
	MinecraftServerGui$2("ma"),
	MinecraftServerGui$3("mb"),
	MinecraftServerGui$4("mc"),
	MinecraftServerGui$5("md"),
	PlayerListComponent("me"),
	StatsComponent("mf"),
	StatsComponent$1("mg"),
	DemoWorldServer("mk"),
	DemoWorldManager("ml"),
	WorldServerMulti("mm"),
	EntityTracker("mn"),
	EntityTracker$1("mo"),
	WorldManager("mp"),
	PlayerManager("mq"),
	PlayerManager$PlayerInstance("mr"),
	ChunkProviderServer("ms"),
	WorldServer("mt"),
	WorldServer$1("mu"),
	WorldServer$ServerBlockEventList("mv"),
	EntityPlayerMP("mw"),
	ItemInWorldManager("mx"),
	EntityTrackerEntry("my"),
	PingResponseHandler("mz"),
	CrashReportCategory$3("n"),
	NetworkSystem("nc"),
	NetworkSystem$1("nd"),
	NetworkSystem$2("ne"),
	DisconnectedOnlineScreen("net/minecraft/realms/DisconnectedOnlineScreen"),
	Realms("net/minecraft/realms/Realms"),
	RealmsAnvilLevelStorageSource("net/minecraft/realms/RealmsAnvilLevelStorageSource"),
	RealmsBridge("net/minecraft/realms/RealmsBridge"),
	RealmsButton("net/minecraft/realms/RealmsButton"),
	RealmsConnect("net/minecraft/realms/RealmsConnect"),
	RealmsEditBox("net/minecraft/realms/RealmsEditBox"),
	RealmsLevelSummary("net/minecraft/realms/RealmsLevelSummary"),
	RealmsMth("net/minecraft/realms/RealmsMth"),
	RealmsScreen("net/minecraft/realms/RealmsScreen"),
	RealmsScrolledSelectionList("net/minecraft/realms/RealmsScrolledSelectionList"),
	RealmsServerAddress("net/minecraft/realms/RealmsServerAddress"),
	RealmsServerStatusPinger("net/minecraft/realms/RealmsServerStatusPinger"),
	RealmsSharedConstants("net/minecraft/realms/RealmsSharedConstants"),
	RealmsSliderButton("net/minecraft/realms/RealmsSliderButton"),
	RendererUtility("net/minecraft/realms/RendererUtility"),
	ServerPing("net/minecraft/realms/ServerPing"),
	Tezzelator("net/minecraft/realms/Tezzelator"),
	MinecraftServer("net/minecraft/server/MinecraftServer"),
	NetworkSystem$3("nf"),
	NetworkSystem$4("ng"),
	NetHandlerPlayServer("nh"),
	NetHandlerPlayServer$1("ni"),
	NetHandlerPlayServer$2("nj"),
	NetHandlerPlayServer$SwitchEnumState("nk"),
	NetHandlerHandshakeTCP("nl"),
	NetHandlerHandshakeTCP$SwitchEnumConnectionState("nm"),
	NetHandlerLoginServer("nn"),
	NetHandlerLoginServer$1("no"),
	NetHandlerLoginServer$LoginState("np"),
	NetHandlerStatusServer("nq"),
	BanEntry("nr"),
	PlayerProfileCache("ns"),
	PlayerProfileCache$1("nt"),
	PlayerProfileCache$2("nu"),
	PlayerProfileCache$ProfileEntry("nv"),
	PlayerProfileCache$Serializer("nw"),
	BanList("nx"),
	IPBanEntry("ny"),
	PreYggdrasilConverter("nz"),
	CrashReportCategory$Entry("o"),
	PreYggdrasilConverter$1("oa"),
	PreYggdrasilConverter$2("ob"),
	PreYggdrasilConverter$3("oc"),
	PreYggdrasilConverter$4("od"),
	PreYggdrasilConverter$5("oe"),
	PreYggdrasilConverter$6("of"),
	PreYggdrasilConverter$7("og"),
	PreYggdrasilConverter$ConversionError("oh"),
	ServerConfigurationManager("oi"),
	UserListOps("oj"),
	UserListOpsEntry("ok"),
	UserListEntry("ol"),
	UserList("om"),
	UserList$1("on"),
	UserList$Serializer("oo"),
	UserListBans("op"),
	UserListBansEntry("oq"),
	UserListWhitelist("or"),
	UserListWhitelistEntry("os"),
	RConOutputStream("ot"),
	RConUtils("ou"),
	RConConsoleSource("ov"),
	RConThreadBase("ow"),
	RConThreadQuery("ox"),
	RConThreadQuery$Auth("oy"),
	RConThreadClient("oz"),
	Direction("p"),
	RConThreadMain("pa"),
	Achievement("pb"),
	AchievementList("pc"),
	IStatStringFormat("pd"),
	StatBasic("pe"),
	StatCrafting("pf"),
	StatisticsFile("pg"),
	StatBase("ph"),
	StatBase$1("pi"),
	StatBase$2("pj"),
	StatBase$3("pk"),
	StatBase$4("pl"),
	IStatType("pm"),
	TupleIntJsonSerializable("pn"),
	IJsonSerializable("po"),
	StatList("pp"),
	StatFileWriter("pq"),
	JsonSerializableSet("pr"),
	CryptManager("pt"),
	JsonUtils("pu"),
	HttpUtil("pv"),
	HttpUtil$1("pw"),
	HttpUtil$DownloadListener("px"),
	LowerStringMap("py"),
	IntHashMap("pz"),
	Facing("q"),
	IntHashMap$Entry("qa"),
	ThreadSafeBoundList("qc"),
	LongHashMap("qd"),
	LongHashMap$Entry("qe"),
	EnumTypeAdapterFactory("qf"),
	EnumTypeAdapterFactory$1("qg"),
	MathHelper("qh"),
	Profiler("qi"),
	Profiler$Result("qj"),
	IProgressUpdate("qk"),
	MouseFilter("qm"),
	StringUtils("qn"),
	Tuple("qu"),
	WeightedRandom("qv"),
	WeightedRandom$Item("qw"),
	WeightedRandomChestContent("qx"),
	ChunkCoordinates("r"),
	InventoryLargeChest("ra"),
	IInventory("rb"),
	IInvBasic("rc"),
	EnumDifficulty("rd"),
	IIcon("rf"),
	InventoryBasic("rh"),
	PlayerUsageSnooper("ri"),
	PlayerUsageSnooper$1("rj"),
	IPlayerUsage("rk"),
	ISidedInventory("rl"),
	CombatEntry("rm"),
	CombatTracker("rn"),
	DamageSource("ro"),
	EntityDamageSource("rp"),
	EntityDamageSourceIndirect("rq"),
	PotionAbsoption("rr"),
	PotionAttackDamage("rs"),
	PotionHealthBoost("rt"),
	PotionHealth("ru"),
	Potion("rv"),
	PotionEffect("rw"),
	EntityAgeable("rx"),
	IAnimals("ry"),
	ReportedException("s"),
	Entity("sa"),
	Entity$1("sb"),
	Entity$2("sc"),
	Entity$SwitchEnumEntitySize("sd"),
	Entity$EnumEntitySize("se"),
	EntityList("sg"),
	EntityList$EntityEggInfo("sh"),
	IEntitySelector("sj"),
	IEntitySelector$1("sk"),
	IEntitySelector$2("sl"),
	IEntitySelector$3("sm"),
	IEntitySelector$ArmoredMob("sp"),
	EntityXPOrb("sq"),
	EntityFlying("sr"),
	EntityHanging("ss"),
	EntityItemFrame("st"),
	EntityLeashKnot("su"),
	EntityLivingBase("sv"),
	EntityLiving("sw"),
	EnumCreatureType("sx"),
	IEntityLivingData("sy"),
	EnumCreatureAttribute("sz"),
	ChatAllowedCharacters("t"),
	IEntityOwnable("ta"),
	EntityPainting("tb"),
	EntityPainting$EnumArt("tc"),
	EntityCreature("td"),
	DataWatcher("te"),
	DataWatcher$WatchableObject("tf"),
	EntityTameable("tg"),
	IAttribute("th"),
	IAttributeInstance("ti"),
	AttributeModifier("tj"),
	BaseAttribute("tk"),
	BaseAttributeMap("tl"),
	ModifiableAttributeInstance("tn"),
	RangedAttribute("to"),
	ServersideAttributeMap("tq"),
	EntityBodyHelper("tr"),
	EntityJumpHelper("tt"),
	EntityLookHelper("tu"),
	EntityMoveHelper("tv"),
	EntityAIAvoidEntity("tw"),
	EntityAIAvoidEntity$1("tx"),
	EntityAIBeg("ty"),
	EntityAIBreakDoor("tz"),
	Util("u"),
	EntityAIMate("ua"),
	EntityAIControlledByPlayer("ub"),
	EntityAIDoorInteract("uc"),
	EntityAIEatGrass("ud"),
	EntityAIFleeSun("ue"),
	EntityAISwimming("uf"),
	EntityAIFollowOwner("ug"),
	EntityAIFollowParent("uh"),
	EntityAIBase("ui"),
	EntityAITasks("uj"),
	EntityAITasks$EntityAITaskEntry("uk"),
	EntityAIWatchClosest2("ul"),
	EntityAILeapAtTarget("um"),
	EntityAIWatchClosest("un"),
	EntityAILookAtTradePlayer("uo"),
	EntityAIVillagerMate("up"),
	EntityAIAttackOnCollide("uq"),
	EntityAIMoveIndoors("ur"),
	EntityAIMoveThroughVillage("us"),
	EntityAIMoveTowardsRestriction("ut"),
	EntityAIMoveTowardsTarget("uu"),
	EntityAIOcelotAttack("uv"),
	EntityAIOcelotSit("uw"),
	EntityAILookAtVillager("ux"),
	EntityAIOpenDoor("uy"),
	EntityAIPanic("uz"),
	Util$EnumOS("v"),
	EntityAIPlay("va"),
	EntityAILookIdle("vb"),
	EntityAIWander("vc"),
	EntityAIArrowAttack("vd"),
	EntityAIRestrictOpenDoor("ve"),
	EntityAIRestrictSun("vf"),
	EntityAIRunAroundLikeCrazy("vg"),
	EntityAISit("vh"),
	EntityAICreeperSwell("vi"),
	EntityAIFollowGolem("vj"),
	EntityAITempt("vk"),
	EntityAITradePlayer("vl"),
	EntityAIDefendVillage("vm"),
	EntityAIHurtByTarget("vn"),
	EntityAINearestAttackableTarget("vo"),
	EntityAINearestAttackableTarget$1("vp"),
	EntityAINearestAttackableTarget$Sorter("vq"),
	EntityAITargetNonTamed("vr"),
	EntityAIOwnerHurtByTarget("vs"),
	EntityAIOwnerHurtTarget("vt"),
	EntityAITarget("vu"),
	PathNavigate("vv"),
	EntitySenses("vw"),
	RandomPositionGenerator("vx"),
	VillageDoorInfo("vy"),
	Village("vz"),
	Village$VillageAgressor("wa"),
	VillageSiege("wb"),
	VillageCollection("wc"),
	EntityAmbientCreature("wd"),
	EntityBat("we"),
	EntityAnimal("wf"),
	EntityChicken("wg"),
	EntityCow("wh"),
	EntityHorse("wi"),
	EntityHorse$1("wj"),
	EntityHorse$GroupData("wk"),
	EntityGolem("wl"),
	EntityMooshroom("wm"),
	EntityOcelot("wn"),
	EntityPig("wo"),
	EntitySheep("wp"),
	EntitySheep$1("wq"),
	EntitySnowman("wr"),
	EntitySquid("ws"),
	EntityIronGolem("wt"),
	EntityWaterMob("wu"),
	EntityWolf("wv"),
	IBossDisplayData("ww"),
	IEntityMultiPart("wx"),
	EntityDragonPart("wy"),
	EntityEnderCrystal("wz"),
	IAdminCommand("x"),
	EntityDragon("xa"),
	EntityWither("xc"),
	EntityWither$1("xd"),
	EntityFishHook("xe"),
	WeightedRandomFishable("xf"),
	EntityWeatherEffect("xg"),
	EntityLightningBolt("xh"),
	EntityBoat("xi"),
	EntityFallingBlock("xj"),
	EntityItem("xk"),
	EntityMinecart("xl"),
	EntityMinecartChest("xm"),
	EntityMinecartCommandBlock("xn"),
	EntityMinecartCommandBlock$1("xo"),
	EntityMinecartContainer("xp"),
	EntityMinecartFurnace("xq"),
	EntityMinecartHopper("xr"),
	EntityMinecartEmpty("xs"),
	EntityMinecartMobSpawner("xt"),
	EntityMinecartMobSpawner$1("xu"),
	EntityMinecartTNT("xv"),
	EntityTNTPrimed("xw"),
	EntityBlaze("xx"),
	EntityCaveSpider("xy"),
	EntityCreeper("xz"),
	CommandBase("y"),
	EntityEnderman("ya"),
	IMob("yb"),
	IMob$1("yc"),
	EntityGhast("yd"),
	EntityGiantZombie("ye"),
	EntityMagmaCube("yf"),
	EntityMob("yg"),
	EntityPigZombie("yh"),
	IRangedAttackMob("yi"),
	SharedMonsterAttributes("yj"),
	EntitySilverfish("yk"),
	EntitySkeleton("yl"),
	EntitySlime("ym"),
	EntitySpider("yn"),
	EntitySpider$GroupData("yo"),
	EntityWitch("yp"),
	EntityZombie("yq"),
	EntityZombie$1("yr"),
	EntityZombie$GroupData("ys"),
	NpcMerchant("yt"),
	INpc("yu"),
	EntityVillager("yv"),
	PlayerCapabilities("yw"),
	InventoryPlayer("yx"),
	InventoryPlayer$1("yy"),
	EntityPlayer("yz"),
	CommandHandler("z"),
	EntityPlayer$EnumStatus("za"),
	EntityPlayer$EnumChatVisibility("zb"),
	EntityArrow("zc"),
	EntityEnderEye("zd"),
	EntityFireball("ze"),
	EntityFireworkRocket("zf"),
	EntityLargeFireball("zg"),
	IProjectile("zh"),
	EntitySmallFireball("zi"),
	EntitySnowball("zj"),
	EntityThrowable("zk"),
	EntityEgg("zl"),
	EntityEnderPearl("zm"),
	EntityExpBottle("zn"),
	EntityPotion("zo"),
	EntityWitherSkull("zp"),
	FoodStats("zr"),
	Container("zs"),
	AnimalChest("zt"),
	ContainerRepair("zu"),
	ContainerRepair$1("zv"),
	ContainerRepair$2("zw"),
	ContainerBeacon("zx"),
	ContainerBeacon$BeaconSlot("zy"),
	ContainerBrewingStand("zz");
	
	public String className;
	Classes(String className){
		this.className = className;
	}
	
	@Override
	public String toString(){
		return className;
	}
}
