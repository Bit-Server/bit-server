package bitserver.nms;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import bitserver.logger.Logger;
import bitserver.nms.patcher.Patcher;
import me.bram0101.javadownloader.MultiThreadDownloader;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public class Loader {

	private static Logger l = Logger.getLogger(Loader.class);

	public Loader(String[] args) {
		File dir = new File("./lib");
		if (!dir.exists())
			dir.mkdir();
		File f = new File(dir, "NMS");
		File fd = new File(dir, "Minecraft_Server.1.7.10.jar");
		if (!f.exists()) {
			l.logln("Minecraft server jar has not been downloaded! Downloading minecraft server jar. This can take a while!");
			try {
				MultiThreadDownloader downloader = new MultiThreadDownloader(
						new URL(
								"https://s3.amazonaws.com/Minecraft.Download/versions/1.7.10/minecraft_server.1.7.10.jar"),
						fd, "NMS Download");
				downloader.run();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			l.logln("Patching Minecraft server jar. This can also take a while!");
			Patcher p = new Patcher();
		}
		if (!f.exists()) {
			l.errorln("Downloaded file not found!");
		}
		if (Patcher.needPatch()) {
			l.logln("A patch update has been released with Bit Server. Downloading a new Minecraft server jar and patching it. This can take a while!");
			l.logln("Downloading minecraft server jar.");
			try {
				MultiThreadDownloader downloader = new MultiThreadDownloader(
						new URL(
								"https://s3.amazonaws.com/Minecraft.Download/versions/1.7.10/minecraft_server.1.7.10.jar"),
						fd, "NMS Download");
				downloader.run();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			l.logln("Patching Minecraft server jar.");
			Patcher p = new Patcher();
		}

		try {
			l.logln("Injecting Minecraft server jar into Bit Server");
			URL myJarFile = f.toURI().toURL();
			URLClassLoader sysLoader = (URLClassLoader) ClassLoader
					.getSystemClassLoader();
			Class<URLClassLoader> sysClass = URLClassLoader.class;
			Method sysMethod = sysClass.getDeclaredMethod("addURL",
					new Class[] { URL.class });
			sysMethod.setAccessible(true);
			sysMethod.invoke(sysLoader, new Object[] { myJarFile });

			URLClassLoader cl = URLClassLoader
					.newInstance(new URL[] { myJarFile });
			Class<?> nms = cl.loadClass("net.minecraft.server.MinecraftServer");
			l.logln("Starting server");
			String[] nargs = new String[args.length + 1];
			for (int i = 0; i < args.length; i++) {
				nargs[i] = args[i];
			}
			nargs[nargs.length - 1] = "nogui";
			try {
				/*
				 * for (Method m : plugin.getMethods()) { if
				 * (m.getName().contains("main")) { Method enable = m;
				 * enable.invoke(plugin.newInstance(), nargs.clone()); break; }
				 * }
				 */
				Method method = nms.getMethod("main", String[].class);
				method.invoke(null, new Object[] { nargs });
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
