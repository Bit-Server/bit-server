package bitserver.nms.patcher;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import at.spardat.xma.xdelta.JarPatcher;
import bitserver.logger.Logger;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

public class Patcher {

	private static Logger l = Logger.getLogger(Patcher.class);

	private static String NMS_VERSION = "1.7.10_4";

	public static boolean needPatch() {
		File dir = new File("./lib");
		File pf = new File(dir, "NMS.PATCHED");
		if (pf.exists()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(pf));
				// if (br.readLine().equalsIgnoreCase(NMS_VERSION)) {
				// return false;
				// } else {
				// return true;
				// }
				{
					CodeSource src = Patcher.class.getProtectionDomain()
							.getCodeSource();
					ZipFile zip = null;
					if (src != null) {
						URL jar = src.getLocation();
						zip = new ZipFile(jar.getFile().replace("%20", " "));
						Enumeration entries2 = zip.entries();
						while (entries2.hasMoreElements()) {
							ZipEntry e = (ZipEntry) entries2.nextElement();
							if (e == null)
								break;
							String name = e.getName();
							if (name.contains("bitserver/nms/patcher/files")) {
								String hash = br.readLine();
								if (hash.equalsIgnoreCase(String.valueOf(e.getTime()))) {
									return false;
								} else {
									l.logln("Old Patch: " + hash);
									l.logln("New Patch: "
											+ String.valueOf(e.getTime()));
									return true;
								}
							}
						}
					} else {
						throw new Exception("can't find jar");
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (Exception ex) {

				}
			}
		} else {
			return true;
		}
		return false;
	}

	public Patcher() {
		try {
			File dir = new File("./lib");
			File f3 = new File(dir, "Minecraft_Server.1.7.10.jar");
			File odir = new File(dir, "NMS");
			File f7 = new File(dir, "Minecraft_Server.patched.jar");
			if (!odir.exists()) {
				odir.mkdir();
			} else {
				delete(odir);
			}

			ZipFile zip = null;
			try {
				File d1 = new File("./tmp");
				d1.mkdir();
				CodeSource src = Patcher.class.getProtectionDomain()
						.getCodeSource();
				if (src != null) {
					URL jar = src.getLocation();
					zip = new ZipFile(jar.getFile().replace("%20", " "));
					Enumeration entries2 = zip.entries();
					while (entries2.hasMoreElements()) {
						ZipEntry e = (ZipEntry) entries2.nextElement();
						if (e == null)
							break;
						String name = e.getName();
						if (name.contains("bitserver/nms/patcher/files")) {
							InputStream is = zip.getInputStream(e);
							if (is != null) {
								File f4 = new File(d1, e.getName().replace(
										"bitserver/nms/patcher/files/", ""));
								if (!e.isDirectory()) {
									BufferedOutputStream os = new BufferedOutputStream(
											new FileOutputStream(
													new File(
															d1,
															e.getName()
																	.replace(
																			"bitserver/nms/patcher/files/",
																			""))));
									byte[] buffer = new byte[1024];
									int length;
									while ((length = is.read(buffer)) > 0) {
										os.write(buffer, 0, length);
									}
									is.close();
									os.close();
								} else {
									f4.mkdir();
								}
							}
						}
					}
				} else {
					throw new Exception("can't find jar");
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					zip.close();
				} catch (Exception ex) {

				}
			}

			File f1 = new File("./tmp");
			for (File f : f1.listFiles()) {
				if (f.isFile()) {
					ZipFile zf1 = new ZipFile(f3);
					ZipFile zf2 = new ZipFile(f);
					ZipOutputStream zos = new ZipOutputStream(
							new FileOutputStream(f7));
					new JarPatcher().applyDelta(zf1, zf2, zos);
					/*
					 * InputStream is = null; OutputStream os = null; try { is =
					 * new FileInputStream(f); String path1 = f .getPath()
					 * .replace(f1.getPath(), "") .replace( "." +
					 * File.pathSeparator + "tmp" + File.pathSeparator, "")
					 * .replace(".PATCH", ".class"); File path2 = new File(odir,
					 * path1); os = new FileOutputStream(path2); byte[] buffer =
					 * new byte[1024]; int length; while ((length =
					 * is.read(buffer)) > 0) { os.write(buffer, 0, length); } }
					 * catch (Exception ex) { ex.printStackTrace(); } finally {
					 * is.close(); os.close(); }
					 */
				}
			}

			ZipFile file = new ZipFile(f3);
			Enumeration entries = file.entries();
			while (entries.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) entries.nextElement();
				if (ze.isDirectory()) {
					new File(odir, ze.getName()).mkdir();
					continue;
				}
				InputStream is = file.getInputStream(ze);
				BufferedOutputStream os = new BufferedOutputStream(
						new FileOutputStream(new File(odir, ze.getName())));
				byte[] buffer = new byte[1024];
				int length;
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
				is.close();
				os.close();
			}
			file.close();
			if (f3.exists()) {
				if (!f3.delete()) {
					l.errorln("Can't delete old minecraft server jar!");
				}
			}
			File pf = new File(dir, "NMS.PATCHED");
			BufferedWriter bw = null;
			try {
				pf.createNewFile();
				bw = new BufferedWriter(new FileWriter(pf));
				{
					CodeSource src = Patcher.class.getProtectionDomain()
							.getCodeSource();
					if (src != null) {
						URL jar = src.getLocation();
						zip = new ZipFile(jar.getFile().replace("%20", " "));
						Enumeration entries2 = zip.entries();
						while (entries2.hasMoreElements()) {
							ZipEntry e = (ZipEntry) entries2.nextElement();
							if (e == null)
								break;
							String name = e.getName();
							if (name.contains("bitserver/nms/patcher/files")) {
								bw.write(String.valueOf(e.getTime()));
								l.logln("Patch hashcode is:"
										+ String.valueOf(e.getTime()));
								break;
							}
						}
					} else {
						throw new Exception("can't find jar");
					}
				}
				// bw.write(NMS_VERSION);
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					bw.close();
				} catch (Exception ex) {

				}
			}
			delete(new File("./tmp"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void delete(File file) {
		if (file.isDirectory()) {
			if (file.list().length == 0) {
				file.delete();
			} else {
				String files[] = file.list();
				for (String temp : files) {
					File fileDelete = new File(file, temp);
					delete(fileDelete);
				}
				if (file.list().length == 0) {
					file.delete();
				}
			}
		} else {
			file.delete();
		}
	}
}
