package bitserver.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public class ReflectionUtil {

	public static Object getField(String clazz, Object clazzInstance,
			String field) {
		try {
			Class c = Class.forName(clazz);
			for (Field f : c.getFields()) {
				if (f.getName().equalsIgnoreCase(field)) {
					if (!f.isAccessible()) {
						f.setAccessible(true);
					}
					return f.get(clazzInstance);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static void setField(String clazz, Object clazzInstance,
			String field, Object data) {
		try {
			Class c = Class.forName(clazz);
			for (Field f : c.getFields()) {
				if (f.getName().equalsIgnoreCase(field)) {
					if (!f.isAccessible()) {
						f.setAccessible(true);
					}
					f.set(clazzInstance, data);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static Object getMethod(String clazz, Object clazzInstance,
			String method, Object... args) {
		try {
			Class c = Class.forName(clazz);
			for (Method m : c.getMethods()) {
				if (m.getName().equalsIgnoreCase(method)) {
					if (args.length == m.getParameterTypes().length) {
						for (int i = 0; i < m.getParameterTypes().length; i++) {
							if (args[i].getClass().getName()
									.equals(m.getParameterTypes()[i].getName())) {
								return m.invoke(clazzInstance, args);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static Object getNewInstance(String clazz, Object... args) {
		try {
			Class c = Class.forName(clazz);
			Class[] cargs = new Class[args.length];
			for(int i = 0; i < args.length; i++){
				cargs[i] = args[i].getClass();
			}
			Constructor co = c.getConstructor(cargs);
			if(co == null){
				throw new Exception("Can't find constructor with the given arguments");
			}
			co.setAccessible(true);
			return co.newInstance(args);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
