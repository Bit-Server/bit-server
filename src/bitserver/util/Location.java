package bitserver.util;

import bitserver.world.World;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public class Location {

	private World world;
	private double x;
	private double y;
	private double z;
	private float pitch;
	private float yaw;

	public Location(World world, double x, double y, double z) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location normalize() {
		double d = (double) Math.sqrt(x * x + y * y + z * z);
		return d < 1.0E-4D ? new Location(world, 0, 0, 0) : new Location(world,
				x / d, y / d, z / d);
	}

	public double dot(Location loc) {
		return x * loc.x + y * loc.y + z * loc.z;
	}

	public Location add(double x, double y, double z) {
		return new Location(world, this.x + x, this.y + y, this.z + z);
	}

	public double distance(Location loc) {
		double x = loc.x - this.x;
		double y = loc.y - this.y;
		double z = loc.z - this.z;
		return (double) Math.sqrt(x * x + y * y + z * z);
	}

	public double distanceSquare(Location loc) {
		double x = loc.x - this.x;
		double y = loc.y - this.y;
		double z = loc.z - this.z;
		return x * x + y * y + z * z;
	}

	public double length() {
		return (double) Math.sqrt(x * x + y * y + z * z);
	}

	public String toString() {
		return "(" + x + ", " + y + ", " + z + ")";
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public void rotateAroundX(float angle) {
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		double x = this.x;
		double y = this.y * (double) cos + this.z * (double) sin;
		double z = this.z * (double) cos - this.y * (double) sin;
		set(x, y, z);
	}

	public void rotateAroundY(float angle) {
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		double x = this.x * (double) cos + this.z * (double) sin;
		double y = this.y;
		double z = this.z * (double) cos - this.x * (double) sin;
		set(x, y, z);
	}

	public void set(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public World getWorld() {
		return world;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

}
