package bitserver.util;

/*
 * Copyright (c) 2014 Bram Stout
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


public class Vector {

	private double x, y, z;

	public Vector(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public Vector copy() {
		return new Vector(x, y, z);
	}

	public double length() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public Vector add(Vector v) {
		x += v.x;
		y += v.y;
		z += v.z;
		return this;
	}

	public Vector substract(Vector v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return this;
	}

	public Vector multiply(double d) {
		x *= d;
		y *= d;
		z *= d;
		return this;
	}

	public Vector multiply(Vector v) {
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return this;
	}

	public Vector devide(double d) {
		x /= d;
		y /= d;
		z /= d;
		return this;
	}

	public Vector devide(Vector v) {
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return this;
	}

	public double dot(Vector v) {
		return x * v.x + y * v.y + z * v.z;
	}

	public Vector cross(Vector v) {
		return new Vector(y * v.z - v.y * z, z * v.x - v.z * x, x * v.y - v.x
				* y);
	}

	public Vector normalize() {
		if (length() > 0) {
			devide(length());
		}
		return this;
	}

	public Vector limit(double max) {
		if (length() > max) {
			normalize();
			multiply(max);
		}
		return this;
	}

	public double distance(Vector v) {
		double dx = x - v.x;
		double dy = y - v.y;
		double dz = z - v.z;
		return Math.sqrt(dx * dx + dy * dy + dz * dz);
	}
}
